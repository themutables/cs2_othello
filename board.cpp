#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns a vector of legal moves for a given side.
 */

std::vector<Move *> Board::getMoves(Side side)
{
	std::vector<Move *> theMoves;
	for (int i = 0; i < 8; i++) // iterate through all possible moves
	{
		for (int j = 0; j < 8; j++)
		{
			Move * move = new Move(i, j);
			if (checkMove(move, side))
			{
				theMoves.push_back(move); // add move if it is valid
			}
			else
			{
				delete move;
			}
		}
	}
	return theMoves; 
}
				

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Given player color, returns heuristic for current board. 
 */ 
int Board::getScore(Side playerType)
{
	
	Side other = (playerType == BLACK) ? WHITE : BLACK;
	int score = 0;
	for (int x = 0; x < 8; x++)
	{
		int multiplier = LAYER_BENEFIT; // weight scores of first row
		if (x == 0 or x == 7)
		{
			multiplier = CORNER_BENEFIT; // weight corners more 
		}
		if (x == 1 or x == 6)
		{
			multiplier = NEAR_CORNER_COST; // and spots next to corners negatively
		}
		score += multiplier*get(playerType, x, 0);
		score -= multiplier*get(other, x, 0); // sum up scores in first row
	}
	for (int x = 0; x < 8; x++)
	{
		int multiplier = LAYER_BENEFIT; // weight scores of last row
		if (x == 0 or x == 7)
		{
			multiplier = CORNER_BENEFIT; // weight corners more
		}
		if (x == 1 or x == 6)
		{
			multiplier = NEAR_CORNER_COST;
		}
		score += multiplier*get(playerType, x, 7);
		score -= multiplier*get(other, x, 7); // sum up scores in last row
	}
	for (int y = 1; y < 7; y++)
	{
		int multiplier = LAYER_BENEFIT; // weight scores of first col
		if (y == 1 or y == 6)
		{
			multiplier = NEAR_CORNER_COST;
		}
		score += multiplier*get(playerType, 0, y);
		score -= multiplier*get(other, 0, y); // sum up scores in first col
	}
	for (int y = 1; y < 7; y++)
	{
		int multiplier = LAYER_BENEFIT; // weight scores of last col
		if (y == 1 or y == 6)
		{
			multiplier = NEAR_CORNER_COST;
		}
		score += multiplier*get(playerType, 7, y);
		score -= multiplier*get(other, 7, y); // sum up scores in last col
	}
	
	score += NEAR_CORNER_COST * get(playerType, 1, 1);
	score -= NEAR_CORNER_COST * get(playerType, 1, 1);
	score += NEAR_CORNER_COST * get(playerType, 6, 1);
	score -= NEAR_CORNER_COST * get(playerType, 6, 1);
	score += NEAR_CORNER_COST * get(playerType, 6, 6);
	score -= NEAR_CORNER_COST * get(playerType, 6, 6);
	score += NEAR_CORNER_COST * get(playerType, 1, 6);
	score -= NEAR_CORNER_COST * get(playerType, 1, 6);
	// weight places next to corners badly 
	
	if (playerType == BLACK) // player is black 
	{
		score += countBlack() - countWhite(); // add base scoring
	}
	else // player is white
	{
		score += countWhite() - countBlack(); // add base scoring 
	}
	return score; 
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
