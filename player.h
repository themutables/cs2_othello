#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    
    int findScore(Board * theBoard, Move * move, Side turn, int depth, int alpha, int beta);
    
    Board GameBoard; 
    
    Side PlayerSide; // whether we are black or white 
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
