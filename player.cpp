#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    PlayerSide = side; // set the side we are playing on 
	
	GameBoard = Board(); // set up the game board
	
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
 


Move *Player::doMove(Move *opponentsMove, int msLeft) {
	// We want the side of the opponent and this is opposite from our
	// current side
	Side other = (PlayerSide == BLACK) ? WHITE : BLACK;
	if (opponentsMove)
	{
		GameBoard.doMove(opponentsMove, other);
		// make opponents move on our board
	}
	std::vector<Move *> daMoves = GameBoard.getMoves(PlayerSide);
	if (daMoves.size() == 0)
	{
		return NULL;
	}
	std::vector<int> scores;
	
	// iterate through possible moves
	for (unsigned int i = 0; i < daMoves.size(); i++)
	{
		// for every move, look into the future to find out how 
		// good the move is 
		Board * copyBoard = GameBoard.copy();
		scores.push_back(findScore(copyBoard, daMoves[i], PlayerSide, 5, INT_MIN, INT_MAX));
		// find score recursively down to a depth
		// and initialize alpha and beta to -/+ infinity
		delete copyBoard;
	}
	int max_score = INT_MIN;
	int index;
	for (unsigned int i = 0; i < scores.size(); i++)
	{
		if (scores[i] > max_score)
		{
			max_score = scores[i];
			index = i;
		}
	}
	
	GameBoard.doMove(daMoves[index], PlayerSide); // make our move
	Move * OurMove = new Move(daMoves[index]->getX(), daMoves[index]->getY());
	// move to return, constructed from the correct move in the moves vector
	daMoves.clear(); // clean up the moves vector
	
	return OurMove; // and return the move 
	
}

/*
 * Recursively finds a score for a given possible move down to some 
 * depth.
 */

int Player::findScore(Board * theBoard, Move * move, Side turn, int depth, int alpha, int beta)
{
	theBoard->doMove(move, turn);
	// first, make the move on the board 
	
	Side other = (turn == BLACK) ? WHITE : BLACK;
	// this is the other side
	
	if (depth == 0) // base case
	{
		// simply score the current board
		return theBoard->getScore(PlayerSide);
		// always get scores relative to our player 
	}
	
	std::vector<Move *> daMoves = theBoard->getMoves(other);
	// get the moves for this turn
	
	if (daMoves.size() == 0) // if can't make valid move
	{
		return theBoard->getScore(PlayerSide);
		// just score the current board
	}
	
	std::vector<int> daScores; // vector of scores
	 
	// iterate through possible moves to get scores
	for (unsigned int i = 0; i < daMoves.size(); i++)
	{
		Board * copyboard = theBoard->copy();
		Side other = (turn == BLACK) ? WHITE : BLACK;

		// need other side for minimax algorithm
		if (alpha > beta)
		{
			break; // prune using alpha-beta
		}
		int score = findScore(copyboard, daMoves[i], other, depth-1, alpha, beta);
		
		if (other != PlayerSide and score > alpha)
		{
			// if recursive call was with opponents turn we got an alpha value
			alpha = score; 
		}
		else if (score < beta)
		{
			// else we got a beta value 
			beta = score; 
		}
		daScores.push_back(score);
		delete copyboard; 
	}
	// use scores vector to get best move
	int index;
	if (turn != PlayerSide) 
	// maximize our score if on opponents turn as we are looking one
	// turn into the future on the function call
	{
		int bestScore = INT_MIN;
		for (unsigned int i = 0; i < daScores.size(); i++)

		{
			if (daScores[i] > bestScore)
			{
				index = i;
				bestScore = daScores[i];
			}
		}
	}
	else // minimize our score if on opponents turn 
	{
		int worstScore = INT_MAX;
		for (unsigned int i = 0; i < daScores.size(); i++)
		{
			if (daScores[i] < worstScore)
			{
				index = i;
				worstScore = daScores[i];
			}
		}
	}
	return daScores[index]; // return the desired score  
}
